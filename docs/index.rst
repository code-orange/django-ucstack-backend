.. Unified Communications Stack Backend documentation master file, created by
   sphinx-quickstart on Mon Oct 21 00:59:01 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Unified Communications Stack Backend's documentation!
================================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
